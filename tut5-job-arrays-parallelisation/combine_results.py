#!/usr/bin/env python3

"""
Combines the partials results from each job array task
"""

import re
import sys

total_trials = 0
total_success_count = 0
for line in sys.stdin:
    match = re.search(r'num_trials=(\d+) success_count=(\d+)', line)
    if not match:
        print('Line is not as expected: ', line, file=sys.stderr)
        sys.exit(1)
    total_trials += int(match.group(1))
    total_success_count += int(match.group(2))

# Calculate and output final estimate, using same formula as before
pi_estimate = 4.0 * total_success_count / total_trials
print(f'Final result: pi=~{pi_estimate} num_trials={total_trials} success_count={total_success_count}')

