#!/bin/bash
#
# Tutorial 5 Slurm script - Monte Carlo Pi Job Array
#
#SBATCH --job-name=tut5_mcp_array
#SBATCH --partition=short
#SBATCH --time=10:00
#SBATCH --array=1-8
#
########################################################################

# Let's output which task we are within the job array
echo "Hello from Slurm job array task: $SLURM_ARRAY_TASK_ID"

# Require the user to pass a single command line
# argument containing the number of trials to be run.
# The special $# variable tells us how many arguments
# were passed, so we check whether this is equal to 1.
if [ $# -ne 1 ]
then
  echo "Required argument: [num_trials]"
  exit 1
fi
num_trials=$1

# Now we launch our Python code in the same way as Tutorial 2.
#
# This time we'll save the result into a bunch of files named
# after the array job ID and tark ID.
# This will make it easier to combine
# the results together later on.
result_file=result-${SLURM_ARRAY_TASK_ID}.txt
echo "Running Monte Carlo code, writing results to $result_file"
./monte_carlo_pi.py $num_trials >$result_file
echo "All done"
