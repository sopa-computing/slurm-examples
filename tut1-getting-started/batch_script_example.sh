#!/bin/bash
#
# Tutorial 1: Basic Slurm example batch script
#
# This script simply runs a program (xthi) to
# do a pointless calculation for 60 seconds.
# We provide xthi on our Ubuntu Linux platform.
#
#SBATCH --job-name=tut1_basic
#SBATCH --partition=short
#SBATCH --time=10:00
#SBATCH --mem=1G

echo "Hello from compute node $HOSTNAME!"
echo "Running xthi to chew a CPU for 60 seconds..."
xthi 60
echo "All done!"

