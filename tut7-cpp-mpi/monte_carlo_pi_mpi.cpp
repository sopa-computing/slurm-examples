#include <exception>
#include <iostream>
#include <random>
#include <mpi.h>

/**
 * Estimation of Pi via Monte Carlo simulation.
 *
 * Parallel version of this code, using MPI.
 */

inline double square(double x) {
    return x * x;
}

template <class Engine>
class MonteCarloPi {
private:
    Engine engine;
    std::uniform_real_distribution<> distribution;
    double nextRandom() { return distribution(engine); }
public:
    explicit MonteCarloPi(unsigned int seed) : engine(Engine(seed)) {}
    long doTrials(const long num_trials) {
        long success_count = 0;
        for (long i = num_trials; i > 0; --i) {
            double x = nextRandom();
            double y = nextRandom();
            if (square(x-0.5) + square(y-0.5) < square(0.5)) {
                ++success_count;
            }
        }
        return success_count;
    }
};

/* Parses num_trials from the command line args.
 * Returns true on successful parse, false otherwise.
 */
bool parse_num_trials(int argc, char* argv[], long& num_trials) {
    if (argc != 2) {
        std::cerr << "Required argument: <number of trials>" << std::endl;
        return false;
    }
    try {
        num_trials = std::stol(std::string(argv[1]), nullptr, 10);
    }
    catch (std::exception& ex) {
        std::cerr << "Cannot parse num_trials: " << ex.what() << std::endl;
        return false;
    }
    if (num_trials < 0) {
        std::cerr << "Number of trials must be a positive long integer" << std::endl;
        return false;
    }
    return true;
}

int main(int argc, char* argv[]) {
    int mpi_size, mpi_rank; // MPI world size & rank
    const MPI_Comm mpi_comm = MPI_COMM_WORLD; // We'll always use the default MPI communicator here
    const int mpi_tag = 0; // We can get away with using a single tag for all MPI communication here

    // Initialize the MPI environment
    // Note how we pass (pointers to) argc & argv to MPI_Init() here.
    MPI_Init(&argc, &argv);
    MPI_Comm_size(mpi_comm, &mpi_size);
    MPI_Comm_rank(mpi_comm, &mpi_rank);

    // Read requested number of trials from command line arguments
    long total_num_trials;
    if (!parse_num_trials(argc, argv, total_num_trials)) {
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // Work out the number of trials to do per process.
    // This is computed with a bit of integer division, which
    // of course rounds downward, so we perform 1 extra trial on
    // some of the ranks to make up the remainder.
    long num_trials_this_rank = total_num_trials / mpi_size;
    if (mpi_rank < total_num_trials % mpi_size) {
        ++num_trials_this_rank;
    }

    // Perform specified number of Monte Carlo trials.
    // We'll use the mt19937_64 Mersenne Twister pseudo-random number generator.
    const unsigned int seed = std::random_device{}();
    auto mcpi = MonteCarloPi<std::mt19937_64>{ seed };
    const long success_count_this_rank = mcpi.doTrials(num_trials_this_rank);

    // Report the partial result for this rank, which is handy for
    // understanding what's going on.
    std::cout << "Partial result for rank=" << mpi_rank
              << ": num_trials=" << num_trials_this_rank
              << " success_count=" << success_count_this_rank
              << std::endl;

    if (mpi_rank != 0) {
        // This is a worker process.
        // We send our partial result to the manager process (0).
        //
        // We use MPI_Ssend() for this, which is the simplest way
        // of sending an MPI message.
        // It sends the message immediately (=synchronous),
        // then waits until the other process receives the
        // message (=blocking).
        //
        // This simple method works fine in this example, but you need
        // to be careful in more complex cases as you can end up with a
        // deadlock.
        MPI_Ssend(&success_count_this_rank, 1, MPI_LONG, 0, mpi_tag, mpi_comm);
    }
    else {
        // This is the manager process.
        // We will receive results from each of the worker processes
        // and tot up our final result (total_success_count), which
        // we'll start off as the number of successes recorded by this
        // process.
        long total_success_count = success_count_this_rank;
        long received_success_count; // Used to receive data from worker processes
        for (int i = 1; i < mpi_size; i++) {
            // Receive next incoming worker process result
            // into received_success_count variable.
            // We use MPI_ANY_SOURCE here to indicate we're happy to
            // receive results from any rank in whichever order to come in -
            // everything should tally up in the end.
            MPI_Recv(&received_success_count, 1, MPI_LONG, MPI_ANY_SOURCE,
                     mpi_tag, mpi_comm, MPI_STATUS_IGNORE);
            // Add the received count to the final total
            total_success_count += received_success_count;
        }

        // Compute our resulting estimate for pi
        const double pi_estimate = 4.0 * (double) total_success_count / (double) total_num_trials;

        // Output final result
        std::cout << "Final result: pi=~" << std::fixed << pi_estimate
                  << " num_trials=" << total_num_trials
                  << " success_count=" << total_success_count
                  << std::endl;
    }

    // Clean up and exit happily
    MPI_Finalize();
    return EXIT_SUCCESS;
}
