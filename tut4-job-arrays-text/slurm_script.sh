#!/bin/bash
#
# Tutorial 4 Job Array Example: "Text Minging"
#
# Each task will process one of the 6 one-line text input files,
# passing it into our (not very magic) rev program, which simply
# writes the line backwards. The result is stored in an output
# file named in the same way as the inputs.
#
#SBATCH --job-name=tut4_array
#SBATCH --partition=short
#SBATCH --time=10:00
#SBATCH --array=1-6
#
########################################################################

# Decide which input file this Slurm task will process.
# We use the special $SLURM_ARRAY_TASK_ID variable, which tells the
# task which one it is among the whole job array.
input_file=input${SLURM_ARRAY_TASK_ID}.txt
output_file=output${SLURM_ARRAY_TASK_ID}.txt

# Now do our "processing" on the input file
echo "Processing input file $input_file and writing to $output_file"
rev $input_file >$output_file

# Finally let's sleep for 60 seconds so that we can watch the tasks
# get processed for the purposes of this tutorial.
sleep 60
echo "All done"
