#!/bin/bash
#
# OpenMP example running 4 threads.
#
#SBATCH --cpus-per-task=4

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
xthi 60
