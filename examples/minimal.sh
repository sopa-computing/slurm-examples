#!/bin/bash
#
# Minimal working example batch script
#
#SBATCH --partition=short
#SBATCH --job-name=minimal
#SBATCH --time=0:10:00

echo "Hello from compute node $HOSTNAME!"
echo "Sleeping for 60 seconds..."
sleep 60
echo "All done"
