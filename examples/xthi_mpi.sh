#!/bin/bash
#
# MPI example using OpenMPI.
#
# Here we're running our xthi example code as 16 parallel MPI tasks.
# Each task will chew its CPU for 60 seconds.
#
# The key SBATCH option we need here is the following.
# This specifies the number of MPI tasks you want to run.
# (You can also override this in your sbatch command.)
#SBATCH --ntasks=16

# Launch our example xthi MPI code in parallel,
# asking each MPI task to chew CPU for 60 seconds.
srun xthi 60

# OR: you can replace 'srun' above with:
# mpirun -np $SLURM_NTASKS ...
# mpirun ...
#
# NOTE for more advanced users: each of these alternatives
# binds the MPI tasks to CPUs in different ways!
