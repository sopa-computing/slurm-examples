#!/bin/bash
#
# Example of running LAMMPS in parallel.
#
# Here we're running the default LAMMPS code (lmp) provided on our
# Ubuntu Linux Platform.
#
# We're running it in parallel over 16 MPI tasks using OpenMPI.
#
# We're using one of the example input files, downloaded from:
# https://lammps.org/inputs/in.lj.txt
# (Though I've modified it to do 1000000 iterations instead of 100)
#
#SBATCH --job-name=lammps_example
#SBATCH --ntasks=16
#SBATCH --partition=short
#SBATCH --time=10:00
#
######################################################################

srun lmp -in in.lj.txt
