#!/bin/bash
#
# Hybrid MPI & OpenMP example.
# This launches 8 MPI tasks, each running 4 OpenMP threads.
# So this job will be allocated 8 x 4 = 32 CPUs in total.
#
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=4

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
srun xthi 60
