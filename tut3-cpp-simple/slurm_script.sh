#!/bin/bash
#
# Tutorial 3 Slurm script.
# This runs our compiled C++ Monte Carlo Pi code.
# The required number of MC trials is passed as an argument.
#
#SBATCH --job-name=tut3_mcp_cpp
#SBATCH --partition=short
#SBATCH --time=10:00
#
########################################################################

# Require the user to pass a single command line
# argument containing the number of trials to be run.
# The special $# variable tells us how many arguments
# were passed, so we check whether this is equal to 1.
if [ $# -ne 1 ]
then
  echo "Required argument: [num_trials]"
  exit 1
fi
num_trials=$1

# Now launch our compiled C++ code.
./monte_carlo_pi $num_trials
