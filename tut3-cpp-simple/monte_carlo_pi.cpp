#include <exception>
#include <iostream>
#include <random>

/**
 * Estimation of Pi via Monte Carlo simulation.
 *
 * Initial (not very) C++ serial version.
 */

inline double square(double x) {
    return x * x;
}

template <class Engine>
class MonteCarloPi {
private:
    Engine engine;
    std::uniform_real_distribution<> distribution;
    double nextRandom() { return distribution(engine); }
public:
    explicit MonteCarloPi(unsigned int seed) : engine(Engine(seed)) {}
    long doTrials(const long num_trials) {
        long success_count = 0;
        for (long i = num_trials; i > 0; --i) {
            double x = nextRandom();
            double y = nextRandom();
            if (square(x-0.5) + square(y-0.5) < square(0.5)) {
                ++success_count;
            }
        }
        return success_count;
    }
};

/* Parses num_trials from the command line args.
 * Returns true on successful parse, false otherwise.
 */
bool parse_num_trials(int argc, char* argv[], long& num_trials) {
    if (argc != 2) {
        std::cerr << "Required argument: <number of trials>" << std::endl;
        return false;
    }
    try {
        num_trials = std::stol(std::string(argv[1]), nullptr, 10);
    }
    catch (std::exception& ex) {
        std::cerr << "Cannot parse num_trials: " << ex.what() << std::endl;
        return false;
    }
    if (num_trials < 0) {
        std::cerr << "Number of trials must be a positive long integer" << std::endl;
        return false;
    }
    return true;
}

int main(int argc, char* argv[]) {
    // Read requested number of trials from command line arguments
    long num_trials;
    if (!parse_num_trials(argc, argv, num_trials)) {
        return EXIT_FAILURE;
    }

    // Perform specified number of Monte Carlo trials.
    // We'll use the mt19937_64 Mersenne Twister pseudo-random number generator.
    const unsigned int seed = std::random_device{}();
    auto mcpi = MonteCarloPi<std::mt19937_64>{ seed };
    const long success_count = mcpi.doTrials(num_trials);

    // Compute our resulting estimate for pi
    const double pi_estimate = 4.0 * (double) success_count / (double) num_trials;

    // Output final result
    std::cout << "pi=~" << std::fixed << pi_estimate
        << " num_trials=" << num_trials
        << " success_count=" << success_count
        << std::endl;

    // Exit happily
    return EXIT_SUCCESS;
}
