#!/usr/bin/env python3
"""
MPI Parallel version of monte_carlo_pi.py, using mpi4py
"""

import sys
from argparse import ArgumentParser
from random import random

from mpi4py import MPI


def do_monte_carlo(num_trials: int) -> int:
    """Runs MC simulation, sampling num_trials times"""
    success_count = 0
    for _ in range(1, num_trials):
        # Generate a point in the unit square bounded by (+/-0.5,+/-0.5)
        (dx, dy) = (random() - 0.5, random() - 0.5)
        # Determine if it lies in the circle of radius 0.5 centred at (0,0)
        # NB: It's faster to do 'dx * dx' than 'dx ** 2'
        if dx * dx + dy * dy < 0.25:
            success_count += 1
    return success_count


def compute_num_trials(num_trials_total: int, mpi_size: int,
                       mpi_rank: int) -> int:
    """Boilerplate even(ish) division of workload logic"""
    num_trials_this_rank = num_trials_total // mpi_size
    remainder = num_trials_total % mpi_size
    if mpi_rank < remainder:
        num_trials_this_rank += 1
    return num_trials_this_rank


def main() -> None:
    # Read in command line arguments
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('num_trials', type=int, help='Number of trials')
    args = parser.parse_args()
    num_trials_total = args.num_trials
    if num_trials_total <= 0:
        print('num_trials must be positive', file=sys.stderr)
        sys.exit(1)

    # Find out which MPI rank this process is
    comm = MPI.COMM_WORLD
    mpi_size = comm.Get_size()
    mpi_rank = comm.Get_rank()

    # Work out how many trials this MPI rank should do
    num_trials_this_rank = compute_num_trials(num_trials_total, mpi_size, mpi_rank)

    # Run Monte Carlo trials and print out partial result
    success_count = do_monte_carlo(num_trials_this_rank)
    print(f'Partial result for rank {mpi_rank}: num_trials={num_trials_this_rank} success_count={success_count}')

    # The final step depends on whether we are the manager process (mpi_rank == 0)
    # or one of the worker processes (mpi_rank > 0).
    if mpi_rank > 0:
        # This is a worker process.
        # So send results to the manager process.
        comm.send(success_count, dest=0, tag=0)
    else:
        # This is the manager process.
        # So receive results from each worker process.
        for _ in range(1, mpi_size):
            received = comm.recv(source=MPI.ANY_SOURCE, tag=0)
            success_count += received

        # Calculate final estimate from:
        # pi = Area of circle / r^2
        # =~ (success_count / num_trials) / (1/2)^2
        # = 4 * success_count / num_trials
        pi_estimate = 4.0 * success_count / num_trials_total

        # Output result
        print(f'Final result: pi=~{pi_estimate} num_trials={num_trials_total} success_count={success_count}')


if __name__ == '__main__':
    main()
