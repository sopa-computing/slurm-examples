#!/bin/bash
#
# Tutorial 6 Slurm script
#
#SBATCH --job-name=tut6_mcp_py_mpi
#SBATCH --partition=short
#SBATCH --time=10:00
#SBATCH --ntasks=8
#
########################################################################

# Require the user to pass a single command line
# argument containing the number of trials to be run.
# The special $# variable tells us how many arguments
# were passed, so we check whether this is equal to 1.
if [ $# -ne 1 ]
then
  echo "Required argument: [num_trials]"
  exit 1
fi
num_trials=$1

# Now launch the Python simulation
srun monte_carlo_pi_mpi.py $num_trials
