#!/bin/bash
#
# Tutorial 2 Slurm Batch Script (version 1).
#
# This batch script launches some simple Python code,
# run with the system default Python 3 interpreter.
#
#SBATCH --job-name=tut2_mcp
#SBATCH --partition=short
#SBATCH --time=10:00
#
########################################################################

# Launch our Python code, which will run with the
# system default Python 3 interpreter.
#
# We'll just let it output its result to the standard output
# stream (stdout), which Slurm captures in your
# slurm-[JOB_ID].out file
./monte_carlo_pi.py 1000000000
