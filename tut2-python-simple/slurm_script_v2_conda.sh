#!/bin/bash
#
# Tutorial 2 Slurm Batch Script (version 2 with Anaconda).
#
# This is a variant of slurm_script_v2.sh. This script runs
# the Python code using the Python interpreter provided by the
# Anaconda 'base' environment, rather than the system default
# Python 3 interpreter.
#
#SBATCH --job-name=tut2_mcp_v2_conda
#SBATCH --partition=short
#SBATCH --time=60:00
#
########################################################################

# Require the user to pass a single command line
# argument containing the number of trials to be run.
# The special $# variable tells us how many arguments
# were passed, so we check whether this is equal to 1.
if [ $# -ne 1 ]
then
  echo "Required argument: [num_trials]"
  exit 1
fi
num_trials=$1

# Now launch our Python code, using the Python interpreter in the
# 'base' conda environment.
#
# The change we make is using 'conda run -n base ...'
conda run -n base ./monte_carlo_pi.py "$num_trials" >result.txt

# Further notes:
# - The usual 'conda activate' command doesn't work in scripts,
#   hence we use the special 'conda run ...' command.
# - To run with another conda environment (e.g. one of your own),
#   simply change 'base' above to the name of the required environment.
