#!/bin/bash
#
# Tutorial 2 Slurm Batch Script (version 2).
#
# This batch script launches some simple Python code,
# run with the system default Python 3 interpreter.
#
# This version 2 batch script allows (requires) the
# user to specify the number of Monte Carlo trials
# via a command line argument, and the output from our
# Python code is saved to a file.
#
#SBATCH --job-name=tut2_mcp_v2
#SBATCH --partition=short
#SBATCH --time=60:00
#
########################################################################

# Require the user to pass a single command line
# argument containing the number of trials to be run.
# The special $# variable tells us how many arguments
# were passed, so we check whether this is equal to 1.
if [ $# -ne 1 ]
then
  echo "Required argument: [num_trials]"
  exit 1
fi
num_trials=$1

# Now launch our Python code, which will run with the
# system default Python 3 interpreter.
# We'll save the output to a file called 'result.txt'
./monte_carlo_pi.py "$num_trials" >result.txt
