#!/usr/bin/env python3
"""
Example Python code for Tutorial 2.

This estimates the value of Pi by Monte Carlo sampling.

Initial serial version of this code, containing
a wee bit of simple Python code optimisation.
(But not using numpy, which would be better still...)

"""

import sys
from argparse import ArgumentParser
from random import random


def do_monte_carlo(num_trials: int) -> int:
    """Runs MC simulation, sampling num_trials times"""
    success_count = 0
    for _ in range(1, num_trials):
        # Generate a point in the unit square bounded by (+/-0.5,+/-0.5)
        (dx, dy) = (random() - 0.5, random() - 0.5)
        # Determine if it lies in the circle of radius 0.5 centred at (0,0)
        # NB: It's faster to do 'dx * dx' than 'dx ** 2'
        if dx * dx + dy * dy < 0.25:
            success_count += 1
    return success_count


def main() -> None:
    # Read in and check command line arguments
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('num_trials', type=int, help='Number of trials')
    args = parser.parse_args()
    num_trials = args.num_trials
    if num_trials <= 0:
        print('num_trials must be positive', file=sys.stderr)
        sys.exit(1)

    # Run Monte Carlo simulation
    success_count = do_monte_carlo(num_trials)

    # Calculate final estimate from:
    # pi = Area of circle / r^2
    # =~ (success_count / num_trials) / (1/2)^2
    # = 4 * success_count / num_trials
    pi_estimate = 4.0 * success_count / num_trials

    # Output result
    print(f'pi=~{pi_estimate} num_trials={num_trials} success_count={success_count}')


if __name__ == '__main__':
    main()
